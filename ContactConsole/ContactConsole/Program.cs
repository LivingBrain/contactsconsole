﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactConsole
{
    public class Program
    {
        static void Main(string[] args)
        {
            /* zalozenia projektowe
            1. Dane bedziemy przechowywac w pamieci
            2. Dodawanie i usuwanie, wyszukiwanie
            3. Przechowywane dane: imie, nazwisko, numer telefonu
            4. Komunikaty : wymagane pole, walidacje danych, typy pol (imie nazwisko -string, nr tel - int), 
            5. Aplikacja dziala, dopoki nie zostanie zamknieta przez uzytkownika
            6. Menu
            7. Unit testy
            */

            /*petla do menu
            wyswietl kontakt
            dodaj kontakt
            znajdz kontakt
            usun kontakt
            wyjdz*/

            Menu menu = new Menu();
            Repository repository = new Repository();
            Contacts contacts = new Contacts(repository);
            menu.mainMenu(contacts);
        }

        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ContactConsole
{
    public class Contacts
    {
        IRepository repository;
        public Contacts(IRepository repository)
        {
            this.repository = repository;
        }

        public void ContactAdd()
        {
            Console.WriteLine("Write contact details");
            Console.Write("Give contact name: ");
            string contactName = Console.ReadLine();
            Console.Write("Give contact SurName: ");
            string contactSurName = Console.ReadLine();
            Console.Write("Give contact phone number: ");
            string contactPhone = Console.ReadLine();
            while (!Regex.IsMatch(contactPhone, @"^\d+$"))
            {
                Console.Clear();
                Console.Write("Phone number must be all numbers.");                                
                Console.Write("Give contact phone number: ");
                contactPhone = Console.ReadLine();
            }
            string contactDetails = $"{contactName} {contactSurName} {contactPhone}";
            Console.Write("Contact {0} will be saved in contact list.", contactDetails);
            Console.ReadKey();
            Console.Clear();

            repository.AddContact(contactDetails);
        }

        public void ContactRemove()
        {
            Console.Clear();
            string contact = FindContact();
            while (contact == null)
            {
                Console.Clear();
                Console.WriteLine("Contact not found.");
                contact = FindContact();
            }

            Console.WriteLine("Contact {0} will be removed. Are you sure? (y)es, (n)o", contact);
            string answer = Console.ReadLine();

            while (answer.Equals("n"))
            {
                contact = FindContact();
                Console.WriteLine("Contact {0} will be removed. Are you sure? (y)es, (n)o", contact);
                answer = Console.ReadLine();
            }
            repository.DeleteContact(contact);
            Console.WriteLine("Contact deleted!");
            Console.Clear();
            
        }

        public void ContactShow()
        {
            var contactList = repository.ShowAllContacts();

            foreach (string contact in contactList)
            {
                Console.Write(contact);
            }
        }

        public void ContactFind()
        {
            Console.Write(FindContact());
            Console.ReadKey();
            Console.Clear();
        }    
        
        private string FindContact()
        {
            Console.Write("Write contact name or phone number: ");
            string contactDetails = Console.ReadLine();
            var contactList = repository.ShowAllContacts();
            string foundContact = null;
            foreach (string contact in contactList)
            {
                if (contact.Contains(contactDetails))
                {
                    foundContact = contact;
                }
            }
            return foundContact;
        }
    }
}

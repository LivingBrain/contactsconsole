﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactConsole
{
    public interface IRepository
    {
        List<string> ShowAllContacts();

        void AddContact(string contact);

        void DeleteContact(string contact);
    }
}

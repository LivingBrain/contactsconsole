﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactConsole
{
    public class Menu : MenusDetails
    {

        public void mainMenu(Contacts contacts)
        {
            bool ifLoop = true;
            while (ifLoop)
            {
                string menuItem = printMenu(mainMenuList);
                if (menuItem.Equals("1"))
                {
                    subMenu(contacts, showContactsMenu);
                }
                else if (menuItem.Equals("2"))
                {
                    subMenu(contacts, findContactsMenu);
                }
                else if (menuItem.Equals("3"))
                {
                    subMenu(contacts, addContactsMenu);
                }

                else if (menuItem.Equals("4"))
                {
                    subMenu(contacts, deleteContactsMenu);
                }
                else
                {
                    Environment.Exit(0);
                }
            }
        }

        public string printMenu(List<string> menuList)
        {
            foreach (string menuItem in menuList)
            {
                Console.WriteLine(menuItem);
            }
            Console.Write("Type menu number: ");
            var consoleKey = Console.ReadLine();
            Console.Clear();
            return consoleKey;
        }

        public void subMenu(Contacts contacts, List<string> subMenu)
        {
            bool ifLoop = true;
            while (ifLoop)
            {
                string menuItem = printMenu(subMenu);
                if (menuItem.Equals("1.1"))
                {
                    contacts.ContactShow();
                }
                else if (menuItem.Equals("2.1"))
                {
                    contacts.ContactFind();
                }
                else if (menuItem.Equals("3.1"))
                {
                    contacts.ContactAdd();
                }
                else if (menuItem.Equals("4.1"))
                {
                    contacts.ContactRemove();
                }
                else
                {
                    mainMenu(contacts);
                }
            }
        }
    }
}

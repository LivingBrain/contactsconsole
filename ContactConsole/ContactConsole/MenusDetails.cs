﻿using System.Collections.Generic;

namespace ContactConsole
{
    public class MenusDetails
    {
        public static List<string> mainMenuList = new List<string>
        {
            "************ Contacts ************",
            "1 Show contacts",
            "2 Find contacts",
            "3 Add contacts",
            "4 Delete contacts",
            "5 Exit",
            "***********************************"
        };

        public static List<string> showContactsMenu = new List<string>
        {
            "******** 1. Show contacts *********",
            "1.1 Show all contacts",
            "1.2 Back",
            "***********************************"
        };

        public static List<string> findContactsMenu = new List<string>
        {
            "******** 2. Find contacts *********",
            "2.1 Find contact",
            "2.2 Back",
            "***********************************"
        };

        public static List<string> addContactsMenu = new List<string>
        {
            "******** 3. Add contacts **********",
            "3.1 Add contact",
            "3.2 Back",
            "***********************************"
        };

        public static List<string> deleteContactsMenu = new List<string>
        {
            "******** 4. Delete contacts *******",
            "4.1 Delete contact",
            "4.2 Back",
            "***********************************"
        };
    }
}
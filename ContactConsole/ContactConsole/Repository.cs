﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactConsole
{
    class Repository : IRepository
    {
        private static List<string> contactList = new List<string> {
            "Living, Brain, 0048666333111",
            "Pawel, Tester, 0048987654321" };

        public void AddContact(string contact)
        {
            contactList.Add(contact);
        }

        public void DeleteContact(string contact)
        {
            contactList.Remove(contact);
        }

        public List<string> ShowAllContacts()
        {
            return contactList;
        }
    }
}
